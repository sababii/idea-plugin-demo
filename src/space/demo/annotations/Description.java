package space.demo.annotations;

public @interface Description {

    String value();
}
