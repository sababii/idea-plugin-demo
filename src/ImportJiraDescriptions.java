import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.application.WriteAction;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class ImportJiraDescriptions extends AnAction {

    private Project project;

    @Override
    public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
        project = anActionEvent.getProject();

        PsiElement element = anActionEvent.getDataContext().getData(DataKeys.PSI_ELEMENT);
        if (element instanceof PsiClass) {
            Runnable action = () -> WriteAction.run(() -> addJiraDescriptions((PsiClass) element));
            CommandProcessor.getInstance().executeCommand(project, action, null, null);
        }
    }

    private void addJiraDescriptions(PsiClass psiClass) {
        Arrays.stream(psiClass.getMethods())
                .filter(method -> method.hasAnnotation("org.junit.Test"))
                .filter(method -> method.hasAnnotation("space.demo.annotations.JiraId"))
                .forEach(this::addAnnotationForMethod);
    }

    private void addAnnotationForMethod(PsiMethod psiMethod) {
        PsiAnnotation jiraIdAnnotation = psiMethod.getAnnotation("space.demo.annotations.JiraId");
        String jiraId = jiraIdAnnotation.findAttributeValue("value").getText();
        //use jira API to find issue and extract summary
        String summary = findJiraSummaryById(jiraId);

        String annotationText = "space.demo.annotations.Description(\"" + summary + "\")";
        PsiAnnotation summaryAnnotation = psiMethod.getModifierList().addAnnotation(annotationText);
        JavaCodeStyleManager.getInstance(project).shortenClassReferences(summaryAnnotation);
    }

    private String findJiraSummaryById(String id) {
        switch (id) {
            case "\"D-01\"":
                return "This is test case description #1";
            case "\"D-04\"":
                return "This is test case description #4";
            default:
                return "hello world";
        }
    }
}
