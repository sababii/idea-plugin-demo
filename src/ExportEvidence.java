import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class ExportEvidence extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent anActionEvent) {
        VirtualFile virtualFile = anActionEvent.getDataContext().getData(DataKeys.VIRTUAL_FILE);
        Path targetFolder = Paths.get("C:\\Personal\\temp");
        File source = new File(virtualFile.getCanonicalPath());
        recursivelyCopyFiles(source, targetFolder);
    }

    private void recursivelyCopyFiles(File source, Path target) {
        Path targetPath = Paths.get(target + File.separator + source.getName());
        if (source.isFile()) {
            try {
                Files.copy(source.toPath(), targetPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                Files.createDirectory(targetPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Arrays.stream(source.listFiles())
                    .forEach(file -> recursivelyCopyFiles(file, Paths.get(target.toString() + File.separator + source.getName())));
        }
    }
}
